package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += delivery(in)
	}
	fmt.Println(result)
}

func delivery(s string) int {
	grid := map[int]map[int]struct{}{
		0: {
			0: struct{}{},
		},
	}

	var x, y, result int

	for _, c := range s {
		switch c {
		case '^':
			y++
		case 'v':
			y--
		case '>':
			x++
		case '<':
			x--
		}
		if _, ok := grid[x]; !ok {
			grid[x] = map[int]struct{}{}
		}
		grid[x][y] = struct{}{}
	}

	for _, g := range grid {
		result += len(g)
	}
	return result
}
