package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "(())",
			want: 0,
		},
		{
			in:   "()()",
			want: 0,
		},
		{
			in:   "(((",
			want: 3,
		},
		{
			in:   "(()(()(",
			want: 3,
		},
		{
			in:   "))(((((",
			want: 3,
		},
		{
			in:   "())",
			want: -1,
		},
		{
			in:   "))(",
			want: -1,
		},
		{
			in:   ")))",
			want: -3,
		},
		{
			in:   ")())())",
			want: -3,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := floor(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
