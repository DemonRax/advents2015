package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2015/util"
)

func main()  {
	strs := util.ReadFile("./input.txt")
	result := floor(strs[0])
	fmt.Println(result)
}

func floor(s string) int {
	result := 0
	for _, c := range s {
		if c == '(' {
			result++
		} else {
			result--
		}
	}
	return result
}
