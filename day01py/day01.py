def floor(s):
    # type: (str) -> int
    result = 0
    for e in s:
        if e == '(':
            result += 1;
        else:
            result -= 1;
    return result


with open('input.txt', 'r') as file:
    data = file.read()

print(floor(data))
