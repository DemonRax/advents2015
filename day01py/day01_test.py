import unittest

from advents2015.day01py.day01 import floor


class TestDay01(unittest.TestCase):

    def test_floor(self):
        test_cases = [
            {
                "name": "(())",
                "want": 0
            },
            {
                "name": "()()",
                "want": 0
            },
            {
                "name": "(((",
                "want": 3
            },
            {
                "name": "(()(()(",
                "want": 3
            },
            {
                "name": "))(((((",
                "want": 3
            },
            {
                "name": "())",
                "want": -1
            },
            {
                "name": "))(",
                "want": -1
            },
            {
                "name": ")))",
                "want": -3
            },
            {
                "name": ")())())",
                "want": -3
            }
        ]

        for test_case in test_cases:
            #            assert test_case["want"] == floor(test_case["name"]), test_case["name"]
            self.assertEqual(test_case["want"], floor(test_case["name"]))
