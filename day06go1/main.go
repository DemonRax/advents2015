package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	field := make([][]bool, 1000)
	for i := 0; i < 1000; i++ {
		field[i] = make([]bool, 1000)
	}
	for _, s := range input {
		lights(field, s)
	}
	fmt.Println(countLights(field))
}

func countLights(field [][]bool) int {
	var c int
	for i := 0; i < len(field); i++ {
		for j := 0; j < len(field); j++ {
			if field[i][j] {
				c++
			}
		}
	}
	return c
}

func lights(field [][]bool, s string) {
	a, x1, y1, x2, y2 := parse(s)
	for i := x1; i <= x2; i++ {
		for j := y1; j <= y2; j++ {
			switch a {
			case toggle:
				field[i][j] = !field[i][j]
			case on:
				field[i][j] = true
			case off:
				field[i][j] = false
			}
		}
	}
}

type action string

const (
	on     action = "on"
	off    action = "off"
	toggle action = "toggle"
)

func parse(s string) (act action, x1, y1, x2, y2 int) {
	strs := strings.Split(s, " ")
	var s1, s2 string
	switch strs[0] {
	case string(toggle):
		act = toggle
		s1 = strs[1]
		s2 = strs[3]
	case "turn":
		act = action(strs[1])
		s1 = strs[2]
		s2 = strs[4]
	}
	s1s := strings.Split(s1, ",")
	s2s := strings.Split(s2, ",")

	x1, _ = strconv.Atoi(s1s[0])
	y1, _ = strconv.Atoi(s1s[1])
	x2, _ = strconv.Atoi(s2s[0])
	y2, _ = strconv.Atoi(s2s[1])
	return
}
