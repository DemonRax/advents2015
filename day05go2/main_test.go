package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "qjhvhtzxzqqjkmpb",
			want: 1,
		},
		{
			in:   "xxyxx",
			want: 1,
		},
		{
			in:   "asasbxyx",
			want: 1,
		},
		{
			in:   "aaa",
			want: 0,
		},
		{
			in:   "uurcxstgmygtbstg",
			want: 0,
		},
		{
			in:   "ieodomkazucvgmuy",
			want: 0,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := niceString(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
