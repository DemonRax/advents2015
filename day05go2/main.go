package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	var count int
	for _, s := range input {
		count += niceString(s)
	}
	fmt.Println(count)
}

func niceString(s string) int {
	var pair, between bool
	for i := 0; i < len(s)-2; i++ {
		first := string(s[i]) + string(s[i+1])
		for j := i + 1; j < len(s)-2; j++ {
			if first == string(s[j+1])+string(s[j+2]) {
				pair = true
			}
		}
		if s[i] == s[i+2] {
			between = true
		}
	}
	if pair && between {
		return 1
	}
	return 0
}
