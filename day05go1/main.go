package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	var count int
	for _, s := range input {
		count += niceString(s)
	}
	fmt.Println(count)
}

func niceString(s string) int {
	vowels := make([]struct{}, 0, len(s))
	var double bool
	for i := 0; i < len(s); i++ {
		switch s[i] {
		case 'a', 'e', 'i', 'o', 'u':
			vowels = append(vowels, struct{}{})
		}
		if i == 0 {
			continue
		}
		switch string(s[i-1]) + string(s[i]) {
		case "ab", "cd", "pq", "xy":
			return 0
		}
		if s[i-1] == s[i] {
			double = true
		}
	}
	if double && len(vowels) >= 3 {
		return 1
	}
	return 0
}
