package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "bgvyzdsv",
			want: 1038736,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := sixZeroHash(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
