package main

import (
	"crypto/md5"
	"fmt"
	"gitlab.com/DemonRax/advents2015/util"
	"strings"
)

func main() {
	input := util.ReadFile("input.txt")
	result := sixZeroHash(input[0])
	fmt.Println(result)
}

func sixZeroHash(s string) int {
	var suffix int
	for {
		suffix++
		hash := fmt.Sprintf("%x", md5.Sum([]byte(fmt.Sprintf("%s%d", s, suffix))))
		if strings.HasPrefix(hash, "000000") {
			return suffix
		}
	}
}
