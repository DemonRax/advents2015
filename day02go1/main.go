package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += wrapper(in)
	}
	fmt.Println(result)
}

func wrapper(s string) int {
	strs := strings.Split(s, "x")
	l, _ := strconv.Atoi(strs[0])
	w, _ := strconv.Atoi(strs[1])
	h, _ := strconv.Atoi(strs[2])
	c1, c2, c3 := l*w, w*h, h*l
	return 2*(c1+c2+c3) + util.Min(c1, c2, c3)
}
