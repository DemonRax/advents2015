package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "^v",
			want: 3,
		},
		{
			in:   "^>v<",
			want: 3,
		},
		{
			in:   "^v^v^v^v^v",
			want: 11,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := deliveries(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
