package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += deliveries(in)
	}
	fmt.Println(result)
}

func deliveries(s string) int {
	grid := map[int]map[int]struct{}{
		0: {
			0: struct{}{},
		},
	}

	var x1, y1, x2, y2, result int
	var robo bool

	for _, c := range s {
		x, y := &x1, &y1
		if robo {
			x, y = &x2, &y2
		}

		switch c {
		case '^':
			*y++
		case 'v':
			*y--
		case '>':
			*x++
		case '<':
			*x--
		}
		if _, ok := grid[*x]; !ok {
			grid[*x] = map[int]struct{}{}
		}
		grid[*x][*y] = struct{}{}
		robo = !robo
	}

	for _, g := range grid {
		result += len(g)
	}
	return result
}
