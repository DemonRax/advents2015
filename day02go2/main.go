package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2015/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += ribbon(in)
	}
	fmt.Println(result)
}

func ribbon(s string) int {
	strs := strings.Split(s, "x")
	l, _ := strconv.Atoi(strs[0])
	w, _ := strconv.Atoi(strs[1])
	h, _ := strconv.Atoi(strs[2])

	sizes := []int{l, w, h}
	sort.Ints(sizes)
	return 2*(sizes[0]+sizes[1]) + l*w*h
}
