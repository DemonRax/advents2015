package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "2x3x4",
			want: 34,
		},
		{
			in:   "1x1x10",
			want: 14,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := ribbon(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
