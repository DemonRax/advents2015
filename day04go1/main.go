package main

import (
	"crypto/md5"
	"fmt"
	"gitlab.com/DemonRax/advents2015/util"
	"strings"
)

func main() {
	input := util.ReadFile("input.txt")
	result := fiveZeroHash(input[0])
	fmt.Println(result)
}

func fiveZeroHash(s string) int {
	var suffix int
	for {
		suffix++
		hash := fmt.Sprintf("%x", md5.Sum([]byte(fmt.Sprintf("%s%d", s, suffix))))
		if strings.HasPrefix(hash, "00000") {
			return suffix
		}
	}
}
