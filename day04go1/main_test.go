package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "abcdef",
			want: 609043,
		},
		{
			in:   "pqrstuv",
			want: 1048970,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := fiveZeroHash(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
